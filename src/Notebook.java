import enums.BrandNotebook;
import enums.ProcessorModel;
import enums.TypeOfNotebook;

import java.util.Objects;

public class Notebook {

    private BrandNotebook  brandNotebook;
    private String model;
    private TypeOfNotebook typeOfNotebook;
    private String yearOfIssue;
    private ProcessorModel processorModel;
    private String ramSize;
    private String hardDiskSpace;
    private String screenDiagonal;
    private String screenResolution; // забыл как цифры в енам записать.
    private double weight;
    private double price;

    public Notebook() { }

    public Notebook(BrandNotebook brandNotebook, String model, TypeOfNotebook typeOfNotebook, String yearOfIssue,
                    ProcessorModel processorModel, String ramSize, String hardDiskSpace, String screenDiagonal,
                    String screenResolution, double weight, double price) {
        this.brandNotebook = brandNotebook;
        this.model = model;
        this.typeOfNotebook = typeOfNotebook;
        this.yearOfIssue = yearOfIssue;
        this.processorModel = processorModel;
        this.ramSize = ramSize;
        this.hardDiskSpace = hardDiskSpace;
        this.screenDiagonal = screenDiagonal;
        this.screenResolution = screenResolution;
        this.weight = weight;
        this.price = price;
    }

    public BrandNotebook getBrandNotebook() {
        return brandNotebook;
    }

    public void setBrandNotebook(BrandNotebook brandNotebook) {
        this.brandNotebook = brandNotebook;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public TypeOfNotebook getTypeOfNotebook() {
        return typeOfNotebook;
    }

    public void setTypeOfNotebook(TypeOfNotebook typeOfNotebook) {
        this.typeOfNotebook = typeOfNotebook;
    }

    public String getYearOfIssue() {
        return yearOfIssue;
    }

    public void setYearOfIssue(String yearOfIssue) {
        this.yearOfIssue = yearOfIssue;
    }

    public ProcessorModel getProcessorModel() {
        return processorModel;
    }

    public void setProcessorModel(ProcessorModel processorModel) {
        this.processorModel = processorModel;
    }

    public String getRamSize() {
        return ramSize;
    }

    public void setRamSize(String ramSize) {
        this.ramSize = ramSize;
    }

    public String getHardDiskSpace() {
        return hardDiskSpace;
    }

    public void setHardDiskSpace(String hardDiskSpace) {
        this.hardDiskSpace = hardDiskSpace;
    }

    public String getScreenDiagonal() {
        return screenDiagonal;
    }

    public void setScreenDiagonal(String screenDiagonal) {
        this.screenDiagonal = screenDiagonal;
    }

    public String getScreenResolution() {
        return screenResolution;
    }

    public void setScreenResolution(String screenResolution) {
        this.screenResolution = screenResolution;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Notebook{" +
                "brandNotebook=" + brandNotebook +
                ", model='" + model + '\'' +
                ", typeOfNotebook=" + typeOfNotebook +
                ", yearOfIssue='" + yearOfIssue + '\'' +
                ", processorModel=" + processorModel +
                ", ramSize='" + ramSize + '\'' +
                ", hardDiskSpace='" + hardDiskSpace + '\'' +
                ", screenDiagonal='" + screenDiagonal + '\'' +
                ", screenResolution='" + screenResolution + '\'' +
                ", weight=" + weight +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notebook notebook = (Notebook) o;
        return Double.compare(notebook.weight, weight) == 0 && Double.compare(notebook.price, price) == 0 && brandNotebook == notebook.brandNotebook && Objects.equals(model, notebook.model) && typeOfNotebook == notebook.typeOfNotebook && Objects.equals(yearOfIssue, notebook.yearOfIssue) && processorModel == notebook.processorModel && Objects.equals(ramSize, notebook.ramSize) && Objects.equals(hardDiskSpace, notebook.hardDiskSpace) && Objects.equals(screenDiagonal, notebook.screenDiagonal) && Objects.equals(screenResolution, notebook.screenResolution);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brandNotebook, model, typeOfNotebook, yearOfIssue, processorModel, ramSize, hardDiskSpace, screenDiagonal, screenResolution, weight, price);
    }
}
