import java.util.Set;

public class Shop {

    private String shopName;
    private String shopAddress;
    private Warehouse warehouse;
    private Set<String> notebookShopWindow;

    public Shop() {}

    public Shop(String shopName, String shopAddress, Warehouse warehouse) {
        this.shopName = shopName;
        this.shopAddress = shopAddress;
        this.warehouse = warehouse;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Set<String> getNotebookShopWindow() {
        return notebookShopWindow;
    }

    public void setNotebookShopWindow(Set<String> notebookShopWindow) {
        this.notebookShopWindow = notebookShopWindow;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "shopName='" + shopName + '\'' +
                ", shopAddress='" + shopAddress + '\'' +
                ", warehouse=" + warehouse +
                ", notebookShopWindow=" + notebookShopWindow +
                '}';
    }
}
