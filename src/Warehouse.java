import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Warehouse {

    private String address;
    private int totalNumberOfGoods;
    private List<Notebook> listAllNotebook;
    private Set<String> SetOfModelsFew;

    public Warehouse() {
    }

    public Warehouse(String address, int totalNumberOfGoods, List<Notebook> listAllNotebook) {
        this.address = address;
        this.totalNumberOfGoods = totalNumberOfGoods;
        this.listAllNotebook = listAllNotebook;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getTotalNumberOfGoods() {
        return totalNumberOfGoods;
    }

    public void setTotalNumberOfGoods(int totalNumberOfGoods) {
        this.totalNumberOfGoods = totalNumberOfGoods;
    }

    public List<Notebook> getListAllNotebook() {
        return listAllNotebook;
    }

    public void setListAllNotebook(List<Notebook> listAllNotebook) {
        this.listAllNotebook = listAllNotebook;
    }

    public Set<String> getSetOfModelsFew() {
        return SetOfModelsFew;
    }

    public void setSetOfModelsFew(Set<String> setOfModelsFew) {
        SetOfModelsFew = setOfModelsFew;
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "address='" + address + '\'' +
                ", totalNumberOfGoods=" + totalNumberOfGoods +
                ", listAllNotebook=" + listAllNotebook +
                ", SetOfModelsFew=" + SetOfModelsFew +
                '}';
    }
}
