package enums;

public enum BrandNotebook {
    APPLE,
    LENOVO,
    DELL,
    ASUS
}
