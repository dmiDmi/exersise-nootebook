package enums;

public enum ProcessorModel {
    CORE_I3,
    CORE_I5,
    CORE_I7,
    CORE_I9

}
