package enums;

public enum TypeOfNotebook {
    UNIVERSAL_NOTEBOOK,
    GAMING_NOTEBOOK,
    ULTRABOOKS,
    NETBOOKS
}
